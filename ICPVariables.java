// ICP implementation
// Copyright (c) 2013, Sherri Goings
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.

import java.io.*;
import java.util.Properties;
import gpjpp.*;

//extension of GPVariables for ICP-specific stuff

public class ICPVariables extends GPVariables {


    public int NumberOfRounds;
    public double Weight;
    public int NumberOfOpponents;
    public boolean SpacialNiching;
    public int PopulationSize;


    public History gameHistory;

    //public null constructor required for stream loading
    public ICPVariables() { /*gets default values*/ }

    //ID routine required for streams
    public byte isA() { return GPObject.USERVARIABLESID; }

    public void createHistory() {
        gameHistory = new History(NumberOfRounds,Weight);
    }

    //get values from properties
    public void load(Properties props) {

        if (props == null)
            return;
        super.load(props);
        NumberOfRounds = getInt(props, "NumberOfRounds", NumberOfRounds);
        Weight = getDouble(props, "Weight", Weight);
        NumberOfOpponents = getInt(props, "NumberOfOpponents", NumberOfOpponents);
        SpacialNiching = getBoolean(props, "SpacialNiching", SpacialNiching);
        PopulationSize = getInt(props, "PopulationSize", PopulationSize);
    }

    //get values from a stream
    protected void load(DataInputStream is)
        throws ClassNotFoundException, IOException,
            InstantiationException, IllegalAccessException {

        super.load(is);
        NumberOfRounds = is.readInt();
        // readDouble
        Weight = is.readDouble();
        NumberOfOpponents = is.readInt();


    }

    //save values to a stream
    protected void save(DataOutputStream os) throws IOException {

        super.save(os);
        os.writeInt(NumberOfRounds);
        os.writeDouble(Weight);
        os.writeInt(NumberOfOpponents);
    }

    //write values to a text file
    public void printOn(PrintStream os, GPVariables cfg) {

        super.printOn(os, cfg);
        os.println("NumberOfRounds           = "+NumberOfRounds);
        os.println("Weight             = "+Weight);
        os.println("NumberOfOpponents           = "+NumberOfOpponents);
    }
}
