// gpjpp example program
// Copyright (c) 1997, Kim Kokkonen
//
// This program is free software; you can redistribute it and/or 
// modify it under the terms of version 2 of the GNU General Public 
// License as published by the Free Software Foundation.
// 
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
// 
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
//
// Send comments, suggestions, problems to kimk@turbopower.com

import java.io.*;
import gpjpp.*;
import java.util.*;

//extend GP for lawn mowing
//class must be public for stream loading; otherwise non-public ok

public class ICPGP extends GP {
    public static LinkedList<ICPGP> bestGPs = new LinkedList();
    public static double bestFitness = 100000000;
    public static int currentIndividualIndex = 0;
    public static ICPGP bestGP;
    public static int sizeOfPopulation;
    public static int currentGen = 0;

    //public null constructor required during stream loading only
    public ICPGP() {}

    //this constructor called when new GPs are created
    ICPGP(int genes) { super(genes); }

    //this constructor called when GPs are cloned during reproduction
    ICPGP(ICPGP gpo) { super(gpo); }

    //called when GPs are cloned during reproduction
    protected Object clone() { return new ICPGP(this); }

    //ID routine required for streams
    public byte isA() { return GPObject.USERGPID; }

    //must override GP.createGene to create LawnGene instances
    public GPGene createGene(GPNode gpo) { return new ICPGene(gpo); }

    //must override GP.evaluate to return standard fitness
    public double evaluate(GPVariables cfg) {

        ICPVariables tcfg = (ICPVariables)cfg;
        double totFit = 0;  
        
        int N = tcfg.NumberOfOpponents;
        int R = tcfg.NumberOfRounds;
        boolean spacial = tcfg.SpacialNiching;
        sizeOfPopulation = tcfg.PopulationSize;
        Random r = new Random();

        double myMove;
        double yourMove;
        int index;
        ICPGP you = null;

        for (int j = 0; j < N; j++){


            //boolean listEmpty = true;
            if (ICPGP.bestGPs.size() > 0){
                index = ICPGP.bestGPs.size() - 1 - (j % ICPGP.bestGPs.size());
                you = ICPGP.bestGPs.get(index);

            }

            tcfg.createHistory();

            for (int i = 0; i < R; i++) {
                
                myMove = ((ICPGene)get(0)).evaluate(tcfg, this);

                if (you == null){
                    yourMove = r.nextDouble();
                } else {
                    yourMove = ((ICPGene)get(0)).evaluate(tcfg, you);
                }

                tcfg.gameHistory.addToHistory(myMove,yourMove);
            }

            totFit += tcfg.gameHistory.calcFitness()[0];
        }

        totFit = totFit/N/R;

        if (cfg.ComplexityAffectsFitness)
            //add length into fitness to promote small trees
            totFit += length()/10000.0;

        if (spacial){
        // spacial niching: play the guy before you
            ICPGP.bestGPs.add(this);

            ICPGP.currentIndividualIndex += 1;

            if (ICPGP.currentIndividualIndex >= sizeOfPopulation - 1){
                ICPGP.currentIndividualIndex = 0;
                ICPGP.currentGen += 1;
            }

        } else{

            if (ICPGP.bestFitness > totFit){
                ICPGP.bestFitness = totFit;
                ICPGP.bestGP = (this);
            }

            
            ICPGP.currentIndividualIndex += 1;
            if (ICPGP.currentIndividualIndex >= sizeOfPopulation - 1){
                ICPGP.currentIndividualIndex = 0;
                ICPGP.currentGen += 1;
                ICPGP.bestFitness = 10000000;
                ICPGP.bestGPs.add(bestGP);
            }
        }

        //return standard fitness
        return totFit;
    }

    //optionally override GP.printOn to print lawn-specific data
    public void printOn(PrintStream os, GPVariables cfg) {
        super.printOn(os, cfg);
    }

    //optionally override GP.drawOn to draw lawn-specific data
    public void drawOn(GPDrawing ods, String fnameBase, 
        GPVariables cfg) throws IOException {

        //store the result trees to gif files
        super.drawOn(ods, fnameBase, cfg); // RECOMMENT THIS TO DRAW AGAIN
    }

}
